<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'É Necessário no mínimo seis caracteres para a senha.',
    'reset' => 'Sua senha foi resetada!',
    'sent' => 'O email para resetar sua senha foi enviado!',
    'token' => 'This password reset token is invalid.',
    'user' => "Não foi possível encontrar um usuário com esse email.",

];
