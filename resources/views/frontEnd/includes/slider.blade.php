@if(count($SliderBanners)>0)

    <section id="featured">
    <!--<div id="mapa">
                <img/>
                <h4 class="pull-right">Mapeamento das Escolas</h4>
                
            </div>-->
        <!-- start slider -->
        <!-- Slider -->
        @foreach($SliderBanners->slice(0,1) as $SliderBanner)
            <?php
            try {
                $SliderBanner_type = $SliderBanner->webmasterBanner->type;
            } catch (Exception $e) {
                $SliderBanner_type = 0;
            }
            ?>
        @endforeach
        <?php
        $title_var = "title_" . trans('backLang.boxCode');
        $details_var = "details_" . trans('backLang.boxCode');
        $file_var = "file_" . trans('backLang.boxCode');
        ?>
        
        @if($SliderBanner_type==0)
            {{-- Text/Code Banners--}}
            <div class="text-center">
                @foreach($SliderBanners as $SliderBanner)
                    @if($SliderBanner->$details_var !="")
                        <div>{!! $SliderBanner->$details_var !!}</div>
                    @endif
                @endforeach
            </div>
        @elseif($SliderBanner_type==1)
            {{-- Photo Slider Banners--}}
            <div id="main-slider" class="flexslider">
                <ul class="slides">
                    @foreach($SliderBanners as $SliderBanner)
                        <li data-delay="6000" data-masterspeed="300" data-transition="pop">
                            <img src="{{ URL::to('uploads/banners/'.$SliderBanner->$file_var) }}"
                                 alt="{{ $SliderBanner->$title_var }}" data-fullwidthcentering="on" alt="Pixma"/>
                            <div class="flex-caption">
                                <div class=" modern_big_bluebg h1 lft tp-caption start"
                                    data-x="100"
                                    data-y="85"
                                    data-speed="700"
                                    data-endspeed="800"
                                    data-start="1000"
                                    data-easing="easeOutQuint"
                                    data-endeasing="easeOutQuint">
                                                @if($SliderBanner->$details_var !="")
                                                    <h3 style="font-size:35px; font-weight:500;">{!! $SliderBanner->$title_var !!}</h3>
                                                @endif
                                </div>
                                @if($SliderBanner->$details_var !="")
                                <div class=" list_slide lfb tp-caption start" 
                                    data-easing="easeOutExpo" 
                                    data-start="1400" 
                                    data-speed="1050" 
                                    data-y="180" 
                                    data-x="100">
                                    <div class="list-slide">
                                        
                                        <h5 class="dblue"><i class="fa fa-arrow-right slide-icon"></i>{!! nl2br($SliderBanner->$details_var) !!}</h5>
                                    </div>
                                </div>
                                @endif
                                @if($SliderBanner->link_url !="")
                                    <a href="{!! $SliderBanner->link_url !!}"
                                       class="btn btn-theme">{{ trans('frontLang.moreDetails') }}</a>
                                @endif
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        @else
            {{-- Video Banners--}}
            <div class="text-center">
                @foreach($SliderBanners as $SliderBanner)
                    @if($SliderBanner->youtube_link !="")
                        @if($SliderBanner->video_type ==1)
                            <?php
                            $Youtube_id = Helper::Get_youtube_video_id($SliderBanner->youtube_link);
                            ?>
                            @if($Youtube_id !="")
                                {{-- Youtube Video --}}
                                <iframe width="100%" height="500" frameborder="0" allowfullscreen
                                        src="https://www.youtube.com/embed/{{ $Youtube_id }}">
                                </iframe>
                            @endif
                        @elseif($SliderBanner->video_type ==2)
                            <?php
                            $Vimeo_id = Helper::Get_vimeo_video_id($SliderBanner->youtube_link);
                            ?>
                            @if($Vimeo_id !="")
                                {{-- Vimeo Video --}}
                                <iframe width="100%" height="500" frameborder="0" allowfullscreen
                                        src="http://player.vimeo.com/video/{{ $Vimeo_id }}?title=0&amp;byline=0">
                                </iframe>
                            @endif
                        @endif
                    @endif
                    @if($SliderBanner->video_type ==0)
                        @if($SliderBanner->$file_var !="")
                            {{-- Direct Video --}}
                            <video width="100%" height="500" controls>
                                <source src="{{ URL::to('uploads/banners/'.$SliderBanner->$file_var) }}"
                                        type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                        @endif
                    @endif
                    @if($SliderBanner->$details_var !="")
                        <div>{!! $SliderBanner->$details_var !!}</div>
                    @endif
                @endforeach
            </div>
    @endif
    <!-- end slider -->
    </section>
@endif